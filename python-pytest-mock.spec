Name:           python-pytest-mock
Version:        1.10.0
Release:        4
Summary:        Thin-wrapper around the mock package for easier use with py.test
License:        MIT
URL:            https://pypi.python.org/pypi/pytest-mock
Source0:        https://files.pythonhosted.org/packages/source/p/pytest-mock/pytest-mock-%{version}.tar.gz
BuildArch:      noarch

%description
This plugin provides a mocker fixture which is a thin-wrapper around the
patching API provided by the mock package.
Besides undoing the mocking automatically after the end of the test,
it also provides other nice utilities.

%package -n     python2-pytest-mock
Summary:        Thin-wrapper around the mock package for easier use with py.test
BuildArch:      noarch
BuildRequires:  python2-devel python2-pytest >= 2.7 python2-mock python2-setuptools_scm
Requires:       python2-pytest >= 2.7 python2-mock
%{?python_provide:%python_provide python2-pytest-mock}

%description -n python2-pytest-mock
This plugin provides a mocker fixture which is a thin-wrapper around the
patching API provided by the mock package.
Besides undoing the mocking automatically after the end of the test,
it also provides other nice utilities.

%package -n     python3-pytest-mock
Summary:        Thin-wrapper around the mock package for easier use with py.test
BuildArch:      noarch
BuildRequires:  python3-devel python3-pytest >= 2.7 python3-setuptools_scm
Requires:       python3-pytest >= 2.7
%{?python_provide:%python_provide python3-pytest-mock}

%description -n python3-pytest-mock
This plugin provides a mocker fixture which is a thin-wrapper around the
patching API provided by the mock package.
Besides undoing the mocking automatically after the end of the test,
it also provides other nice utilities.

%prep
%autosetup -n pytest-mock-%{version} -p1
rm -rf *.egg-info

%build
%py2_build
%py3_build


%install
%py3_install
%py2_install


%check
PYTHONPATH="$(pwd)" py.test-%{python2_version} test_pytest_mock.py
PYTHONPATH="$(pwd)" py.test-%{python3_version} test_pytest_mock.py

%files -n python2-pytest-mock
%doc README.rst
%license LICENSE
%{python2_sitelib}/pytest_mock-%{version}-py%{python2_version}.egg-info/
%{python2_sitelib}/pytest_mock.py*
%{python2_sitelib}/_pytest_mock_version.py*


%files -n python3-pytest-mock
%doc README.rst
%license LICENSE
%{python3_sitelib}/pytest_mock-%{version}-py%{python3_version}.egg-info/
%{python3_sitelib}/pytest_mock.py*
%{python3_sitelib}/_pytest_mock_version.py*
%{python3_sitelib}/__pycache__/*

%changelog
* Wed Jan 08 2019 yangjian<yangjian79@huawei.com> - 1.10.0-4
- Package init
